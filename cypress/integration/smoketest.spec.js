/// <reference types="Cypress" />
// Smoke test suite that verifies functionality and performance of https://yournextcar.youi.com.au

const url = 'https://yournextcar.youi.com.au'
const data = require('../fixtures/data.json') // load in test data from the fixtures/data.json file

// Randomly generate a test user name that's unique enough to traceback if required
var name = 'AUTOTST' + Math.random().toString(36).slice(4)

// Timer variables to get load time of final page
var count1 = 0;
var counter1;

// Declare functions
function iterateOptions(array, n) {
    var i;
    for (i = 0; i < array.length; i++) {
        cy.get(`#${array[i]}`).check({ force: true })
    }

    var selection = Math.floor(Math.random() * n)
    cy.get(`#${array[selection]}`).check({ force: true })
}

// Nested the form entry test in a function for reusability
function formEntry() {
    it(`Enters in name successfully`, function () {
        // Have to use force true as pointer event is set to none on HTML element
        cy.get('.ync-text-input').click({ force: true }).type(name)
    })

    it('Checks that each life option is steerable', function () {
        iterateOptions(data.lifeOptions, 7)
    })

    it('Checks that each who option is steerable', function () {
        iterateOptions(data.whoOptions, 4)
        iterateOptions(data.furOptions, 2)
    })

    it('Checks that each who and fur babies option is steerable', function () {
        iterateOptions(data.whoOptions, 4)
        iterateOptions(data.furOptions, 2)
    })

    it('Checks that each personal option is steerable', function () {
        iterateOptions(data.age, 4)
        iterateOptions(data.gender, 3)
    })
}

// Begin Tests 
describe('Your Next Car: YNC001', function () {
    it('Asserts that https://yournextcar.youi.com.au returns status 200 (OK)', function () {
        cy.request(url).then((resp) => {
            cy.wrap(resp.status).should('equal', data.status);
        })
    })
})

describe('Your Next Car: YNC002', function () {
    it('Tests if the initial pages loads < 1000ms', function () {
        cy.request(url).then((resp) => {
            cy.wrap(resp.duration).should('be.lessThan', data.intialLoadTime)
        })
    })
})

// Yes Path
describe(`Your Next Car: YNC003 - YES Scenario`, function () {
    // In the event of cypress encountering an uncaught:exception, do not fail the test
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    before(`Visit ${url}`, function () {
        cy.visit(url)
        cy.get('.ync-button--welcome').click()
    })

    // Enter form data
    formEntry()

    it('Checks that YES option is steerable', function () {
        // Need to test for both yes/no scenarios here - added option variable to steer this
        cy.get(`#${data.car[0]}`).check({ force: true })

        // Select random car
        cy.get('.ync-select')
            .first()
            .find('option')
            .then(opt => {
                const optionCount = Cypress.$(opt).length;
                var option = Math.floor(Math.random() * optionCount)

                cy.get('.ync-select').first().children('option').eq(option).then((element) => {
                    cy.get('.ync-select').first().select(element.text())
                });
            })

        // Select random model
        cy.get('.ync-select')
            .last()
            .find('option')
            .then(opt => {
                const optionCount = Cypress.$(opt).length;
                var option = Math.floor(Math.random() * optionCount)

                cy.get('.ync-select').last().children('option').eq(option).then((element) => {
                    cy.get('.ync-select').last().select(element.text())
                });
            })
    })

    it('Validates that results are displayed < 500ms', function () {
        cy.get('.ync-next-button').last().click()
        cy.wait(500) // wait 500ms then assert that we can see the content grid, else final page failed to meet the load time
        cy.get('.ync-content-grid').should('be.visible')
    })

    it('Validates that search results are successfully displayed', function () {
        cy.get('.ync-content-grid__title').should('contain', name)
        cy.get('.ync-content-grid').children().should('have.length.greaterThan', 0) // verify that we get more than 0 results
    })
})

// No Path
describe(`Your Next Car: YNC004 - NO Scenario`, function () {
    // In the event of cypress encountering an uncaught:exception, do not fail the test
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    before(`Visit ${url}`, function () {
        cy.visit(url)
        cy.get('.ync-button--welcome').click()
    })

    formEntry()

    it('Checks that NO option is steerable', function () {
        // Need to test for both yes/no scenarios here - added option variable to steer this
        cy.get(`#${data.car[1]}`).check({ force: true })
        iterateOptions(data.use, 8)
        iterateOptions(data.style, 8)
        cy.get('#budget').as('range').invoke('val', Math.floor(Math.random() * 5))
    })

    it('Validates that results are displayed < 500ms', function () {
        cy.get('.ync-next-button').last().click()

        // wait 500ms then assert that we can see the content grid, else final page failed to meet the load time
        cy.wait(500)
        cy.get('.ync-content-grid').should('be.visible')
    })

    it('Validates that search results are successfully displayed', function () {
        cy.get('.ync-content-grid__title').should('contain', name)
        cy.get('.ync-content-grid').children().should('have.length.greaterThan', 0) // verify that we get more than 0 results 
    })
})