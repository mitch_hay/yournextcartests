# YourNextCarTests

A suite of smoke tests for the https://yournextcar.youi.com.au web application utilising cypress.io

## Source Code

https://bitbucket.org/mitch_hay/yournextcartests/src/master/cypress/integration/smoketest.spec.js

## How to run

- Clone the git repository using **git clone**

- You will need to install Cypress, you can do so by running **npm install cypress --save-dev** in the project folder
    - You can get a rather detailed installation tutorial on https://docs.cypress.io/guides/getting-started/installing-cypress.html

- If it's your first time opening Cypress, type in the console **npx cypress open** 
    - Once open you can close the cypress application 

- Within the repository directory, run command **npx cypress run**
    - Please note that this will execute the suite of tests in Electron, a variant of Google Chrome

## Time taken 

This is logged in the commits, but I've used up the total of 4hrs over two days (Saturday Morning/Afternoon + Sunday Morning) - If I'm being honest, I believe I have spent a little bit more time on the task than assigned, I enjoy programming so I'll admit I got a little carried away... However I have not spent an excessive amount of time, and believe corrections and tidy ups can still be made to my code (see reflection). 

### Reflection/Design Notes

I selected to utilise Cypress as my test automation tool for this task as it's a relatively new open source framework based on JavaScript. Rather than relying on various libraries, Cypress utilises its own DOM manipulation and runs directly in browser (either Chrome or Electron) with no network communication; allowing for there to be no input latency between commands. Unlike other open-source tools, there is no need to establish a framework either, and given the time constraint of the task I considered this tool to be most fitting as it comes with an out-of-the-box framework to utilise. 

If you're interested in learning more, you can do so at https://cypress.io

**My Approach**

I initially manually tested the website to get a feel for the flow and functionality, I tried various combinations to evaluate how many test scenarios to cater for. In turn I decided to utilise the following tests:

- Health Check (verifies that server request status is OK 200)

- Response Time (verifies that the server responds in under 1000ms)

- Form Scenario A: YES (verifies results based on user specifying car make and model)
    - Each checkbox option on the form page is interactable (I've utilised a dynamic approach, where a random set of data is utilised located in *cypress/fixtues/data.json*) 
    - Results load in under 500ms
    - Results displayed are greater than zero (we actually have a page populated with search results)

- Form Scenario B: NO (verifies results based on user not specifying car make and model)
    - Each checkbox option on the form page is interactable (I've utilised a dynamic approach, where a random set of data is utilised located in *cypress/fixtues/data.json*)
    - Results load in under 500ms
    - Results displayed are greater than zero (we actually have a page populated with search results)

**Defects** 

I noticed a couple of things whilst executing this task, mainly the fact that the name field has no constraints on it (a user can enter alphanumeric, and special characters), in conjunction with this, there is currently no character limit set on the free text name field. 

I also found that on the initial page, the "Let's go!" button displays on hover before the animation finishes playing (see: issues/letsgodefect.jpg). I know it's a low/minor cosmetic issue but thought I'd point it out.

**Reflection**

If I had more time to pursue this task, I would work on the stability and robustness of the tests. Right now 100% of them pass, however, I had to stub out a piece of code to ignore the undefined errors I encountered when entering in data into the form fields. They require more investigation to narrow down what is occuring, I'd like to resolve this somehow, although my first theory is that Cypress may be executing the tests too fast (average execution time ~20s) so I would more than likely add some dynamic waits based on the visibility of various elements (inputs, buttons etc.) and this may resolve the overall issue.

I would also do a bit of clean-up to my code, perhaps optimise various parts of it to be more robust, and reuseable. In conjunction with this, I believe I could also add more coverage, maybe verify that the results displayed for the yes path match the car make/model selected etc.

I'm unsure of how your current CI/CD structure works, but once stabalised, I'd look at pairing this with Docker and something like Jenkins/TeamCity where it can be used to test on multiple environments within a containerised structure and triggered based on **git push** commands 

Read more: https://www.docker.com/

Thank you for this opportunity